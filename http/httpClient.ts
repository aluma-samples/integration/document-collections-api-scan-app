import type { TokenRetriever } from "./tokenRetriever";
import type {AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";

export interface HttpClient {
    request(config: HttpClientConfig): Promise<AxiosResponse<any, any>>;
}

export type HttpClientConfig = AxiosRequestConfig

/**
 * Wraps an AxiosInstance in our HttpClient interface.
 */
export class AxiosHttpClient implements HttpClient {
    private readonly axios: AxiosInstance;

    constructor(axios: AxiosInstance) {
        this.axios = axios;
    }

    async request(config: HttpClientConfig): Promise<AxiosResponse<any, any>> {
        if (!config.validateStatus) {
            // disable throwing on http 300+
            config.validateStatus = () => true;
        }
        return await this.axios.request(config);
    }
}

/**
 * AuthenticatingHttpClient requests an access token from the provided
 * AdminService before using it to authenticate further requests.
 */
export class AuthenticatingHttpClient implements HttpClient {
    private readonly tokenRetriever: TokenRetriever;
    private readonly http: HttpClient;

    constructor(tokenRetriever: TokenRetriever, http: HttpClient) {
        this.tokenRetriever = tokenRetriever;
        this.http = http;
    }

    async request(config: HttpClientConfig): Promise<AxiosResponse<any>> {
        config.headers = config.headers || {};
        config.headers[
            "Authorization"
            ] = `Bearer ${await this.tokenRetriever.retrieveToken()}`;

        return await this.http.request(config);
    }
}

export class AuthHeadersAddingHttpClient implements HttpClient {
    private readonly http: HttpClient;
    private account_id: string;
    private api_client_id: string;
    private api_client_max_docs: any;

    constructor(account_id: string, api_client_id: string, api_client_max_docs: number, http: HttpClient) {
        this.account_id = account_id;
        this.api_client_id = api_client_id;
        this.api_client_max_docs = api_client_max_docs;
        this.http = http;
    }

    async request(config: HttpClientConfig): Promise<AxiosResponse<any>> {
        config.headers = config.headers || {};
        config.headers["X-Auth-AccountId"] = this.account_id;
        config.headers["X-Auth-ClientId"] = this.api_client_id;
        config.headers["X-Auth-ClientMaxDocumentsCount"] = this.api_client_max_docs;
        return await this.http.request(config);
    }
}

export class ResponseCheckingHttpClient implements HttpClient {
    constructor(
        private responseChecker: HttpResponseChecker,
        private underlying: HttpClient
    ) {}

    async request(config: HttpClientConfig): Promise<AxiosResponse<any>> {
        const response = await this.underlying.request(config);
        const err = this.responseChecker.checkResponse(
            response.status,
            response.data
        );

        if (err) {
            throw err;
        }

        return response;
    }
}

export interface HttpResponseChecker {
    checkResponse(statusCode: number, httpBody: any): Error | null;
}

/**
 * WaivesResponseChecker tries to deserialise json-encoded errors from Waives,
 * and provide useful errors from them.
 */
export class AlumaResponseChecker implements HttpResponseChecker {
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    checkResponse(statusCode: number, httpBody: any): Error | null {
        if (statusCode < 400) {
            return null;
        }

        try {
            // try to deserialise into a basic waives error first...
            const waivesError = new WaivesError();
            const detailedError = new WaivesDetailedError();
            this.toInstance(waivesError, httpBody);

            if (waivesError.message.length > 0) {
                // json parsed ok, a 'basic' waives error
                return waivesError;
            }

            // not a 'basic' waives error, try a detailed error
            this.toInstance(detailedError, httpBody);
            if (detailedError.status != 0) {
                return detailedError;
            }
        } catch {
            // http body isn't json, ignore
        }
        return new HttpError(statusCode);
    }

    // eslint-disable-next-line @typescript-eslint/ban-types
    private toInstance<T>(obj: T, jsonOrObj: string | object): T {
        // eslint-disable-next-line @typescript-eslint/ban-types
        let jsonObj: object;

        if (typeof jsonOrObj === "string") {
            jsonObj = JSON.parse(jsonOrObj);
        } else {
            jsonObj = jsonOrObj;
        }

        for (const propName in jsonObj) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            obj[propName] = jsonObj[propName];
        }

        return obj;
    }
}

class HttpError extends Error {
    constructor(public statusCode: number) {
        super();

        this.name = `HTTP response error`;
        this.message = `unexpected response code ${statusCode}`;
    }
}

class WaivesError extends Error {
    name = "Waives response error";
    message = "";
}

export class WaivesDetailedError extends Error {
    name = "Waives response error";

    errors = [];
    type = "";
    title = "";
    status = 0;
    instance = "";
    detail = "";

    get message(): string {
        if (this.detail.length > 0) {
            return this.detail;
        }
        return this.title;
    }
}
