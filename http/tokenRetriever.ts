import type {HttpClient} from "./httpClient";

export interface TokenRetriever {
    retrieveToken(): Promise<string>;
}

/**
 * Class which retrieves access tokens for use with waives, and
 * caches them for as long as they're valid.
 */
export class CachingTokenRetriever implements TokenRetriever {
    private readonly http: HttpClient;
    private tokenPromise: Promise<string> | null = null;
    private client_id: string;
    private client_secret: string;

    constructor(http: HttpClient, client_id: string, client_secret: string) {
        this.http = http;
        this.client_id = client_id;
        this.client_secret = client_secret;
    }

    async retrieveToken(): Promise<string> {
        if (!this.isRequestInProgress() || !await this.isTokenValid()) {
            this.tokenPromise = this.doRetrieveToken();
        }

        try {
            return await this.getAccessToken();
        } catch (e) {
            this.tokenPromise = null;
            throw e;
        }
    }

    private async doRetrieveToken(): Promise<string> {
        const response = await this.http.request({
            method: "POST",
            url: "/oauth/token",
            data: {
                client_id: this.client_id,
                client_secret: this.client_secret,
                grant_type: "client_credentials"
            }
        });

        return response.data.access_token as string;
    }

    private async getAccessToken(): Promise<string> {
        if (!this.tokenPromise) {
            throw new Error("Request not in progress");
        }
        return await this.tokenPromise;
    }

    private isRequestInProgress(): boolean {
        return this.tokenPromise != null;
    }

    private async isTokenValid(): Promise<boolean> {
        const token = await this.getAccessToken();
        if (!token) {
            return false
        }
        return token != "" && !this.isTokenExpired(token);
    }

    private isTokenExpired(token: string): boolean {
        const jwt = this.parseJwt(token);

        const expiry = new Date(jwt.exp * 1000);
        return expiry.getTime() < Date.now();
    }

    // credit: https://stackoverflow.com/a/38552302
    private parseJwt(token: string): JwtToken {
        const base64Url = token.split(".")[1];
        const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
        const jsonPayload = decodeURIComponent(
            atob(base64)
                .split("")
                .map(function (c) {
                    return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
                })
                .join("")
        );

        return JSON.parse(jsonPayload);
    }
}

interface JwtToken {
    // issuer (who created and signed the token)
    iss: string;
    // subject (who the token refers to)
    sub: string;
    // audience (who/what the token is intended for)
    aud: string;
    // issued at (seconds since epoch)
    iat: number;
    // expiry (seconds since epoch)
    exp: number;

    // authorised party (to which this token was issued)
    azp: number;

    // token type (eg client-credentials)
    gty: number;
}
