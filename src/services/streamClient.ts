import type {HttpClient} from "../../http/httpClient";
import {
    ClientAction,
    CompleteClientActionResponse,
    CreateStreamResponse,
    StartClientActionResponse,
    StreamResponse
} from "@/services/streamResponse";

export class StreamClient {
    private readonly http: HttpClient;

    constructor(http: HttpClient) {
        this.http = http;
    }

    async create(fileCollectionId: string, projectId: string): Promise<CreateStreamResponse> {
        const response = await this.http.request({
            method: "POST",
            url: "/streams",
            data: {
                file_collection_id: fileCollectionId,
                project_id: projectId,
            }
        })

        return response.data as CreateStreamResponse;
    }

    async startClientAction(
        streamId: string,
        action: ClientAction
    ): Promise<StartClientActionResponse> {
        const response = await this.http.request({
            method: "PUT",
            url: `/streams/${streamId}/start_client_action`,
            params: {action: action}
        })

        return response.data as StartClientActionResponse
    }

    async completeClientAction(
        streamId: string,
        action: ClientAction,
        data: CompleteClientActionResponse
    ):
        Promise<void> {
        await this.http.request({
            method: "PUT",
            url: `/streams/${streamId}/complete_client_action`,
            data: data,
            params: {action: action}
        })
    }

    async get(streamId: string, wait : number, desiredStatus: string): Promise<StreamResponse> {
        const response = await this.http.request({
            method: "GET",
            url: `/streams/${streamId}?wait=${wait}&desired_status=${desiredStatus}`
        })

        return response.data as StreamResponse
    }
}
