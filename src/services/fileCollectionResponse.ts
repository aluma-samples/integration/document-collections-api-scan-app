export interface CreateFileCollectionResponse {
    id: string;
    files: File[];
}

export interface File {
    id: string
}


