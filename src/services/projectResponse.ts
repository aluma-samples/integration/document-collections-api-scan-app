export interface GetProjectsResponse {
    projects: Project[];
}

export interface Project {
    id:             string;
    name:           string;
    document_types: DocumentType[];
}

export interface DocumentType {
    id:             string;
    name:           string;
    extractor_name: string;
    fields:         Field[];
    output_files:   OutputFiles;
}

export interface Field {
    name: string;
}

export interface OutputFiles {
    files: OutputFile[];
}

export interface OutputFile {
    settings: Settings;
    location: Location;
}

export interface Location {
    folder:   string;
    filename: string;
}

export interface Settings {
    format?: string;
}




