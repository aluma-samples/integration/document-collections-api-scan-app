import {
    CreateStreamResponse,
    ExportCompleteData, ExportStartData,
    ValidationStartData,
    StreamResponse,
    ValidateCompleteData,
    ClientAction
} from "@/services/streamResponse";
import {StreamClient} from "@/services/streamClient";
import {streamService} from "@/services/services";


export class StreamService {
    private streamClient: StreamClient;

    constructor(streamClient: StreamClient) {
        this.streamClient = streamClient;
    }

    async get(streamId: string, wait = 0, desiredStatus = ""): Promise<StreamResponse> {
        return await this.streamClient.get(streamId, wait, desiredStatus);
    }

    async waitForStartClientActionState(streamId: string) : Promise<ClientAction> {
        // eslint-disable-next-line no-constant-condition
        while (true) {
            // Wait for up to 5 seconds for the stream to go to the pending:start_client_action state
            // If the stream goes into that state the request returns immediately
            // If the stream completes, fails, times-out or is cancelled then the request returns immediately
            // If 5 seconds elapses then the details of the stream are returned anyway
            const response = await this.streamClient.get(streamId, 5, "pending:start_client_action");

            // Check the actual state
            if (response.state === "pending:start_client_action") {
                // Check the next client action is one we support
                if (response.client_action == "validation" || response.client_action == "export") {
                    // stream is ready for validation
                    return response.client_action;
                } else {
                    throw new Error(`unknown client_action: ${ response.client_action }`);
                }
            } else if (response.state === "completed" || response.state === "failed" || response.state === "cancelled" || response.state === "timed-out") {
                throw new Error(`The stream finished unexpectedly with state: ${ response.state }`)
            } else if (response.state === "pending:complete_client_action") {
                // Another client must be running and have picked up this stream
                throw new Error(`The stream is being processed by another client. The state is: ${ response.state }`)
            }
        }
    }

    async create(fileCollectionId: string, projectId: string): Promise<CreateStreamResponse> {
        return await this.streamClient.create(fileCollectionId, projectId);
    }

    async startValidation(streamId: string): Promise<ValidationStartData> {
        return await this.streamClient.startClientAction(streamId, "validation") as ValidationStartData
    }

    async completeValidation(streamId: string, validatedData: ValidateCompleteData): Promise<void> {
        await this.streamClient.completeClientAction(streamId, "validation", validatedData)
    }

    async startExport(streamId: string): Promise<ExportStartData> {
        return await this.streamClient.startClientAction(streamId, "export") as ExportStartData
    }

    async completeExport(streamId: string, exportData: ExportCompleteData): Promise<void> {
        await this.streamClient.completeClientAction(streamId, "export", exportData)
    }
}
